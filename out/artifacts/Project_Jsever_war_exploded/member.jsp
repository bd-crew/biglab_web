<%--
  Created by IntelliJ IDEA.
  User: axzsw
  Date: 2019-12-25
  Time: 오후 3:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>Big Data Lab.</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
<!-- 이거 엑티브 모양 빼고는 대부분 반복이라서 header에 넣어서 같이 돌린 뒤에 코딩으로 해결해보자 --!>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target = "#myNavbar"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.html">Big Data Lab.</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/dh/indexing">Main / Lab. Intro</a></li>
            <li class="active"><a href="${pageContext.request.contextPath}/dh/member">Member</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/source">achievement / source</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/photo">photo</a></li>
        </ul>
    </div>
</nav>
<br><br><br><br>

<div class="container">
    <h1 class="page-header">연구원 소개</h1>

    <div class="row">
        <div class="col-sm-2">
            <img src="${pageContext.request.contextPath}/resource/member1.jpg" alt="윤교홍 지도교수 사진" class="img-thumbnail">
        </div>
        <div class="col-sm-8">
            <h3>윤홍원</h3>
            <h4>지도 교수 / 전체 프로젝트 컨설팅</h4>
            <p>박사를 거치시고 신라대학교 교수님으로써 지식과 지혜를 가르치기 위해서 Big Data Lab.을 설립하였습니다.</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-2">
            <img src="${pageContext.request.contextPath}/resource/member2.png" alt="김준한 연구원 사진" class="img-thumbnail">
        </div>
        <div class="col-sm-8">
            <h3>김준한</h3>
            <h4>사장 / 프로젝트 전체 코드 담당</h4>
            <p>강력한 코드계의 신라대 거장 연구원입니다. 초기 멤버이자 여러가지로 활동했습니다.</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-2">
            <img src="${pageContext.request.contextPath}/resource/member3.jpg" alt="강민석 연구원 사진" class="img-thumbnail">
        </div>
        <div class="col-sm-8">
            <h3>강민석</h3>
            <h4>부장 / GUI 담당 및 코드 담당</h4>
            <p>문과의 감성으로 코드를 짭니다. 많은</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-2">
            <img src="${pageContext.request.contextPath}/resource/member4.jpg" alt="이동현 연구원 사진" class="img-thumbnail">
        </div>
        <div class="col-sm-8">
            <h3>이동현</h3>
            <h4>팀장 / 코드 담당 및 연구실 정리 담당 및 웹사이트 담당 및 서버 담당</h4>
            <p>겉은 단순하며 속은 복잡하면서 아름답게 구동되는 코드를 짜야하는데.. 후하</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-2">
            <img src="${pageContext.request.contextPath}/resource/member5.jpg" alt="천지연 연구원 사진" class="img-thumbnail">
        </div>
        <div class="col-sm-8">
            <h3>천지연</h3>
            <h4>대리 / 코드 담당</h4>
            <p>코드 잘 짬</p>
        </div>
    </div>
    <jsp:include page="/footer.jsp" flush="true"/>
</body>
</body>
</html>
