<%--
  Created by IntelliJ IDEA.
  User: axzsw
  Date: 2019-12-25
  Time: 오후 3:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Big Data Lab.</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <!-- 스타일 시트 -->
    <style type="text/css">
        .jumbotron {
            background-image: url("${pageContext.request.contextPath}/resource/main3.jpg");
            background-size: cover;
            text-shadow: black 0.2em 0.2em 0.2em;
            color: white;
        }
    </style>
</head>

<body>
<!-- 이거 엑티브 모양 빼고는 대부분 반복이라서 header에 넣어서 같이 돌린 뒤에 코딩으로 해결해보자 -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target = "#myNavbar"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="dh/indexing">Big Data Lab.</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            <li class="active"><a href="${pageContext.request.contextPath}/dh/indexing">Main / Lab. Intro</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/member">Member</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/source">achievement / source</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/photo">photo</a></li>
        </ul>
    </div>
</nav>

<br><br><br><br>
<!-- 점보트론 큰 광고판 같은 이미지 -->
<div class="container">
    <div class = "jumbotron">
        <h1>Big Data Lab.</h1>
        <p>신라대학교 컴퓨터정보공학부 데이터 분석 및 데이터 과학 연구실</p>
        <p><a class="btn btn-primary btn-lg" href="https://gitlab.com/bd-crew" role="button">연구 성과 및 자료 공개</a></p>
    </div>
</div>
<br>
<!-- 연구실 소개 -->
<div class="container">
    <h2 class="page-header">연구실 소개</h2>
    <p>2018년 1학기를 시점으로 신라대학교 컴퓨터정보공학부에서 만들어 졌으며, 데이터 분석과 데이터 과학을 관련하여서 공부하고
        여러가지 프로젝트를 통해서 성과를 내는 세상에 단 하나 뿐인 연구실입니다.</p>
    <p>데이터 분석과 과학을 학습하기 위해서 프로젝트를 하면서 학습과 성과를 동시에 내고 있습니다.</p>
</div>

<!-- 현재 멤버 테이블 -->
<div class="container">
    <h2 class="page-header">현재 멤버</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>이름</th>
            <th>직책</th>
            <th>자기 소개</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>윤홍원</th>
            <th>지도교수 / 전체 프로젝트 컨설팅</th>
            <th>...</th>
        </tr>
        <tr>
            <th>김준한</th>
            <th>4학년 / 전체 code 담당</th>
            <th>미남 김준한입니다.</th>
        </tr>
        <tr>
            <th>강민석</th>
            <th>3학년 / GUI 담당 및 코드 담당</th>
            <th>문과와 감성이 공존하는 강민석입니다.</th>
        </tr>
        <tr>
            <th>이동현</th>
            <th>3학년 / code 및 서버, 웹, 담당</th>
            <th>보기엔 단순하게 안은 복잡하며 아름다운 이동현입니다.</th>
        </tr>
        <tr>
            <th>천지연</th>
            <th>3학년 / code 및 웹 담당</th>
            <th>잘 부탁드립니다.</th>
        </tr>
        </tbody>
    </table>
</div>
<jsp:include page="/footer.jsp" flush="true"/>
</body>
</html>