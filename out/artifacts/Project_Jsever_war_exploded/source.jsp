<%--
  Created by IntelliJ IDEA.
  User: axzsw
  Date: 2019-12-25
  Time: 오후 3:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Big Data Lab.</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
<!-- 이거 엑티브 모양 빼고는 대부분 반복이라서 header에 넣어서 같이 돌린 뒤에 코딩으로 해결해보자 --!>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target = "#myNavbar"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.html">Big Data Lab.</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/dh/indexing">Main / Lab. Intro</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/member">Member</a></li>
            <li class="active"><a href="${pageContext.request.contextPath}/dh/source">achievement / source</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/photo">photo</a></li>
        </ul>
    </div>
</nav>
<br><br><br><br>

<div class="container">
    <h1 class="page-header">연구 성과 자료 및 공유 파일</h1>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>id</th>
            <th>연구 발표명</th>
            <th>업로드 날짜</th>
        </tr>
        </thead>
        <tr>
            <th>1</th>
            <th><a href="https://gitlab.com/bd-crew/ocean">빅데이터 처리에 의한 유해 적조 분석</a></th>
            <th>2019-07-12</th>
        </tr>
        <tr>
            <th>2</th>
            <th><a href="https://gitlab.com/bd-crew/ocean">빅데이터 기반 적조 예측 시스템 구현</a></th>
            <th>2019-11-27</th>
        </tr>
        <tr>
            <th>3</th>
            <th><a href="">한국인 건강검진 빅데이터 분석(웹사이트없음)</a></th>
            <th>2019-11-27</th>
        </tr>
    </table>
</div>
<jsp:include page="/footer.jsp" flush="true"/>
</body>
</body>
</html>