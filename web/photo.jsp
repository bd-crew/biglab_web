<%--
  Created by IntelliJ IDEA.
  User: axzsw
  Date: 2019-12-25
  Time: 오후 3:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Big Data Lab.</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
<!-- 이거 엑티브 모양 빼고는 대부분 반복이라서 header에 넣어서 같이 돌린 뒤에 코딩으로 해결해보자 --!>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target = "#myNavbar"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.html">Big Data Lab.</a>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/dh/indexing">Main / Lab. Intro</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/member">Member</a></li>
            <li><a href="${pageContext.request.contextPath}/dh/source">achievement / source</a></li>
            <li class="active"><a href="${pageContext.request.contextPath}/dh/photo">photo</a></li>
        </ul>
    </div>
</nav>
<br><br><br><br>

<div class="container">
    <h1 class="page-header">연구원 행보</h1>
    <div class="row">

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="${pageContext.request.contextPath}/resource/hang1.jpg" alt="연구소 문 앞 사진">

                <div class="caption">
                    <h3>연구실 문 앞 사진</h3>
                    <p>연구실 문 앞 사진이다. 여러가지로 굉장하다.</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="${pageContext.request.contextPath}/resource/main1.jpg" alt="연구원 다같이 사진">

                <div class="caption">
                    <h3>연구원 사진</h3>
                    <p>연구실에서 다같이 찍은 사진이다.</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="${pageContext.request.contextPath}/resource/main2.jpg" alt="햄찌">

                <div class="caption">
                    <h3>연구실을 반기는 햄찌</h3>
                    <p>연구실 배게이자 여러가지 용도로(?) 활용하는 햄찌이다.</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="${pageContext.request.contextPath}/resource/health.jpg" alt="운동하는 연구원들">

                <div class="caption">
                    <h3>운동하는 연구원들</h3>
                    <p>운동하는 연구원들이다. 이렇게 건강도 챙긴다.</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="${pageContext.request.contextPath}/resource/main3.jpg" alt="이동현 연구실 자리">

                <div class="caption">
                    <h3>이동현 연구원 자리</h3>
                    <p>연구실 자리입니다. 여러 연구원들이 자리를 내새웠습니다.</p>
                </div>
            </div>
        </div>

    </div>

</div>
<jsp:include page="/footer.jsp" flush="true"/>
</body>
</body>
</html>
