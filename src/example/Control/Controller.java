package example.Control;

import javax.servlet.RequestDispatcher;
import java.io.IOException;
import java.util.List;

public class Controller extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        //파라미타 설정
        request.setCharacterEncoding("utf-8");

        //dispatch request tot processing component
        String pathInfo = request.getPathInfo();
        String viewName = "index.jsp";

        if(pathInfo.equals("/indexing")){
            viewName = "index.jsp";
        }
        else if(pathInfo.equals("/member")){
            viewName = "member.jsp";
        }
        else if(pathInfo.equals("/source")){
            viewName = "source.jsp";
        }
        else if(pathInfo.equals("/photo")){
            viewName = "photo.jsp";
        }

        //forward to view component
        StringBuilder sb = new StringBuilder(viewName);
        sb.insert(0,"../");
        RequestDispatcher view = request.getRequestDispatcher(sb.toString());
        view.forward(request,response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doPost(request,response);
    }
}
